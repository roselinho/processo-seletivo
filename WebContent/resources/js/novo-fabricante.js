const router = new VueRouter({
  mode: 'history',

})

var inicio = new Vue({
   el: "#novo-fabricante",
   data: function (){
	    return initialState();
   },
   created: function () {
      let vm = this;
      vm.buscaFabricantes();
   },
   methods: {
      buscaFabricantes: function () {
         const vm = this;
         axios.get("/mercado/rs/fabricantes/listarFabricantes")
            .then(response => {
               console.log('response', response);
               vm.fabricantes = response.data;
            }).catch(function (error) {
               vm.mostraAlertaErro("Erro interno", "Não foi possível listar natureza de serviços");
            }).finally(function () {
            });
      },
      adicionarFabricante: function () {
         const vm = this;
         axios.post("/mercado/rs/fabricantes/adicionarFabricante", {
            nome: vm.fabricante.nome
         })
            .then(response => {
               alert(response.data);
               vm.reload();
               vm.buscaFabricantes();
            }).catch(function (error) {
               alert(error.response.data);
            }).finally(function () {
            });
      },
      estadoInicial: function() {
    	  this.$data = initialState();
	},
    reload: function() {    
    	const vm = this;
    	console.log(router)
    	router.go(router.currentRoute)
     }
   }
});

function initialState() {
	  return {
	      fabricantes: [],
	      fabricante: {
	         nome: ""
	      }
	   }
}