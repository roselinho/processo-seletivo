const router = new VueRouter({
  mode: 'history',

})

var inicio = new Vue({
   el: "#novo-produto",
   data: function (){
	    return initialState();
   },
   created: function () {
      let vm = this;
      vm.buscaProdutos();
      vm.buscaFabricantes();
   },
   methods: {
      buscaProdutos: function () {
         const vm = this;
         axios.get("/mercado/rs/produtos/listarProdutos")
            .then(response => {
               console.log('response', response);
               vm.produtos = response.data;
            }).catch(function (error) {
               vm.mostraAlertaErro("Erro interno", "Não foi possível listar natureza de serviços");
            }).finally(function () {
            });
      },
      buscaFabricantes: function () {
         const vm = this;
         axios.get("/mercado/rs/fabricantes/listarFabricantes")
            .then(response => {
               console.log('response', response);
               vm.fabricantes = response.data;
            }).catch(function (error) {
               vm.mostraAlertaErro("Erro interno", "Não foi possível listar natureza de serviços");
            }).finally(function () {
            });
      },
      adicionarProduto: function () {
         const vm = this;
         axios.post("/mercado/rs/produtos/adicionarProduto", {
            nome: vm.produto.nome,
            fabricante: vm.produto.fabricante,
            volume: vm.produto.volume,
            unidade: vm.produto.unidade,
            estoque: vm.produto.estoque
         })
            .then(response => {
               alert(response.data);
               vm.reload();
               vm.buscaProdutos();
            }).catch(function (error) {
               alert(error.response.data);
            }).finally(function () {
            });
      },
      estadoInicial: function() {
    	  this.$data = initialState();
	},
    reload: function() {    
    	const vm = this;
    	console.log(router)
    	router.go(router.currentRoute)
     }
   }
});

function initialState() {
	  return {
	      produtos: [],
	      fabricantes: [],
	      produto: {
	         nome: "",
	         fabricante: {},
	         volume: 0,
	         unidade: "",
	         estoque: 0
	      }
	   }
}