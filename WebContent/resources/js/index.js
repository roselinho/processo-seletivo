var inicio = new Vue({
	el:"#inicio",
    data: {
        listaProdutos: [],
        fabricantes: [],
        seen: false,
	      produto: {
		         nome: "",
		         fabricante: {},
		         volume: 0,
		         unidade: "",
		         estoque: 0
		      },
        listaProdutosHeader: [
			{sortable: false, key: "nome", label:"Nome"},
			{sortable: false, key: "fabricante.nome", label:"Fabricante"},
			{sortable: false, key: "volume", label:"Volume"},
			{sortable: false, key: "unidade", label:"Unidade"},
			{sortable: false, key: "estoque", label:"Estoque"}
		]
    },
    created: function(){
        let vm =  this;
        vm.buscaProdutos();
    },
    methods:{
        buscaProdutos: function(){
			const vm = this;
			axios.get("/mercado/rs/produtos/listarProdutos")
			.then(response => {
				console.log('response', response);
				vm.listaProdutos = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi possível listar natureza de serviços");
			}).finally(function() {
			});
		},
        excluirProdutos: function(id){
			const vm = this;
			axios.delete("/mercado/rs/produtos/" + id)
			.then(response => {
				alert(response.data);
				vm.buscaProdutos();
			}).catch(function (error) {
				alert("Erro interno: Não foi possível deletar o produto ");
			}).finally(function() {
			});
		},
        showEditForm: function(produto){
			const vm = this;
			vm.seen = true;
			vm.produto = produto;
			
		},
        editarProduto: function(id, produto){
			const vm = this;
			axios.put("/mercado/rs/produtos/" + id, {
				id: id,
	            nome: vm.produto.nome,
	            fabricante: vm.produto.fabricante,
	            volume: vm.produto.volume,
	            unidade: vm.produto.unidade,
	            estoque: vm.produto.estoque
	         })
			.then(response => {
				alert(response.data);
				vm.buscaProdutos();
			}).catch(function (error) {
				alert("Erro interno: Não foi possível deletar o produto ");
			}).finally(function() {
				
			});
			},
		cancelaEditarProduto: function() {
			const vm = this;
			vm.seen = false;
		}
    	}
	})